const fs = require('fs') // NodeJS native filesystem module

//FUNCTIONS
module.exports = {

    /*
    // Verifies if the "requestheaders.log" file exists and creates the file if it doesn't
    requestlogCheck: function(requestfilecheck){ // filecheck doesn't need to be called in this funcion cuz server.js defines it as a global variable -- this was just for the IDE to stop bothering me

        fs.readFile('logs/requestheaders.log', 'utf8', (err, file) => {
        if (err)
            requestfilecheck = false;
        else
            requestfilecheck = true;
        });

        if (requestfilecheck == false){
            console.log("File \"requestheaders.log\" does not exist.\nCreating file \"requestheaders.log\"...");
            fs.writeFile('logs/requestheaders.log', "Client requests Log:\n", (err) => {
                if (err)
                    console.log("Error while trying to create file \"requestheaders.log\": " + err);
                else
                    console.log("File \"requestheaders.log\" created successfully!");
            });
        }
    },
*/
     // Logs client request headers to requestheaders.log
     logHeader: function(req, thisDate){
    
        let headers = JSON.stringify(req.headers);
        fs.appendFile('logs/requestheaders.log', thisDate + "\n-------------------------\n" + req.url + "\n" + headers + "\n\n", (err) => {
        if (err)
            throw err;
        });
        console.log("New log entry")

    }

};