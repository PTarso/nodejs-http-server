const http = require('http');           // Import nodeJS core module
const fs = require('fs');               // Import file system module
const lib = require('./functions')      // Import my custom functions file

// GLOBAL VARIABLES
var filecheck = true;                   // Boolean for chekcing access.log
var requestfilecheck = true;            // Boolean for checking requestheaders.log
var currentDate = new Date();           // Date
var thisDate = currentDate.getUTCDate() + "/" + currentDate.getUTCMonth() + "/" + currentDate.getUTCFullYear() + " --- " + currentDate.getUTCHours() + ":" + currentDate.getUTCMinutes() + ":" + currentDate.getUTCSeconds() + " UTC";


/* BEGIN EXECUTION */

// Check for requestheaders.log
//lib.requestlogCheck();

var server = http.createServer(function(req, res) {// Creating server

    // Handle incomming requests here...
    // Serve pages using objects (TODO)

/*
    req.url = {

        '/': fs.readFile("pages/index.html", function (err, data) {
            res.writeHead(200, {'Content-type': 'text/html'});
            res.write(data);
            res.end();
        }),

        '/info': fs.readFile("pages/info.html", function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html' });
            res.write(data);
            res.end();
        }),

        '/admin': fs.readFile("pages/admin.html", function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html' });
            res.write(data);
            res.end();
        })

    };
    */
    
    if (req.url == '/') {

        // Set respective response header
        fs.readFile("pages/index.html", function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html' });
            res.write(data);
            res.end();
        });

    }

    else if (req.url == '/info')
    {

        // Set respective response header
        fs.readFile("pages/info.html", function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html' });
            res.write(data);
            res.end();
        });
        
    }
    
    else if(req.url == '/admin') 
    {

        // Set respective response header
        fs.readFile("pages/admin.html", function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html' });
            res.write(data);
            res.end();
        });

    }

    else
    {
        res.end("Page not found");
    }

    // Logs request into requestheaders.log
    lib.logHeader(req, thisDate);
});

server.listen(5000); // Listen for any incomming requests on port 5000
console.log("Server is running at port 5000");